package in.nozama.service.user.util;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class PasswordBCryptUtil {

	public static char[] encodePassword(String password) {
		String salt = BCrypt.gensalt();
		String hash = BCrypt.hashpw(password, salt);
		return hash.toCharArray();
	}

	public static boolean verifyPassword(char[] hashPassword, String rawPassword) {
		char[] rawPasswordHash = PasswordBCryptUtil.encodePassword(rawPassword);

		return (hashPassword == rawPasswordHash);

	}

}
