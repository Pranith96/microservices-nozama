package in.nozama.service.user.controller;

import in.nozama.service.model.LoginCredentials;
import in.nozama.service.model.*;
import in.nozama.service.user.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/user", produces = "application/hal+json")
public class UserController {

	@Autowired
	UserService userService;
	
	HttpHeaders headers;

	@GetMapping(value = "/all")
	public ResponseEntity<List<User>> getAllUsers() {
		return ResponseEntity.ok(userService.getAllUser());
	}

	@GetMapping("/{id}")
	public User getUserById(@PathVariable(name = "id") Long userId) {
		return userService.getUserById(userId).get();
	}
	
	@PostMapping("/signup")
	public ResponseEntity<Void> addUser(@RequestBody User user) {
		//Encoding the password using BCrypt Encoder
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		boolean isAdded = userService.addNewUser(user);
		headers = new HttpHeaders();

		return (isAdded) ? new ResponseEntity<Void>(headers, HttpStatus.CREATED)
				: new ResponseEntity<Void>(headers, HttpStatus.NO_CONTENT);
	}

	@PostMapping("/login")
	public ResponseEntity<JwtAuthenticationToken> login(@RequestBody LoginCredentials loginCredentials) {
		boolean isValidUser;
		isValidUser= userService.validateUser(loginCredentials);
		
		return (isValidUser)? new ResponseEntity<>(
				new JwtAuthenticationToken(Jwts.builder().setSubject(loginCredentials.getEmail()).claim("roles", "user")
						.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "nozama").compact()),
				HttpStatus.OK)
				: new ResponseEntity<>(new JwtAuthenticationToken(Jwts.builder().setSubject(loginCredentials.getEmail()).claim("roles", "user")
						.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "nozama").compact()), HttpStatus.NO_CONTENT);
}
}
