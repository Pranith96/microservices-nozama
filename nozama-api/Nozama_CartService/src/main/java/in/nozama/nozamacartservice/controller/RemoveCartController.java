package in.nozama.nozamacartservice.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nozama.nozamacartservice.service.RemoveCartService;
import in.nozama.service.exception.CartNotFoundException;
import in.nozama.service.model.Item;

@RestController
@RequestMapping("/cart")
public class RemoveCartController {
	private static final Logger LOGGER = LoggerFactory.getLogger(RemoveCartController.class);

	@Autowired
	RemoveCartService removeCartService;

	/**
	 * This RemoveCart is for removing the Item from the cart by using ItemId
	 */
	@DeleteMapping(value = "/remove/{itemId}")

	public ResponseEntity<String> removeItemFromCart(@PathVariable("itemId") Long itemId) throws CartNotFoundException {

		LOGGER.info("before removing item from cart in RemoveCartController");
		List<Item> removeItem = removeCartService.removeItemFromCart(itemId);

		if (removeItem.isEmpty()) {

			LOGGER.info("No item found in cart in RemoveCartController");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No item found in cart to Delete");

		} else {

			LOGGER.info("after removing item from cart in RemoveCartController");
			return ResponseEntity.status(HttpStatus.OK).body("Item Deleted Succefully from cart");

		}
	}

	@ExceptionHandler
	void cartNotFound(CartNotFoundException cnfe, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), cnfe.getMessage());
	}
}
