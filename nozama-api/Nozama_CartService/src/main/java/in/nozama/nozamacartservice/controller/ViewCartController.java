package in.nozama.nozamacartservice.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nozama.nozamacartservice.service.ViewCartService;
import in.nozama.service.exception.CartNotFoundException;
import in.nozama.service.model.Item;

@RestController
@RequestMapping("/cart")
public class ViewCartController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewCartController.class);

	@Autowired
	ViewCartService myCartService;

	/**
	 * ViewCart is for viewing the Item in the cart by using ItemId
	 */
	@GetMapping(value = "/mycart/{itemId}")
	public ResponseEntity<List<Item>> viewCart(@PathVariable("itemId") Long itemId) throws CartNotFoundException {

		LOGGER.info("Before viewing the items in cart in ViewCartController ");
		List<Item> responseCartItem = myCartService.viewCart(itemId);

		if (responseCartItem.isEmpty()) {

			LOGGER.info("No Item found in cart in ViewCartController ");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseCartItem);
		} else {

			LOGGER.info("After viewing the cart in ViewCartController ");
			return ResponseEntity.status(HttpStatus.OK).body(responseCartItem);
		}

	}

	@ExceptionHandler
	void cartNotFound(CartNotFoundException cnfe, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), cnfe.getMessage());
	}
}