package in.nozama.nozamacartservice.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import in.nozama.nozamacartservice.service.AddCartService;
import in.nozama.service.exception.InsufficientStockException;
import in.nozama.service.exception.ItemServiceException;
import in.nozama.service.exception.ProductNotFoundException;
import in.nozama.service.model.Item;
import in.nozama.service.model.Product;
import in.nozama.service.util.ServiceUrlBuilder;

@RefreshScope
@RestController
@RequestMapping("/cart")
public class AddCartController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AddCartController.class);

	@Autowired
	AddCartService addCartService;

	@Value("${nozama.api.gateway.serviceid}")
	private String nozamaApiGateway;

	@Autowired
	private EurekaClient eurekaClient;

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * This AddCartController is for adding item into the cart with
	 * productId.Getting product details by calling product service through
	 * Resttemplate and updating the stock quantity simultaneously in the product
	 * service.
	 * 
	 */
	@PostMapping(value = "/add")
	public ResponseEntity<Item> addCart(@RequestBody Item item)
			throws ProductNotFoundException, InsufficientStockException, ItemServiceException {
		Long productId = item.getProduct().getProductId();
		Item responseItem = null;
		// Rest call for product service getById method for getting product details
		Application application = eurekaClient.getApplication(nozamaApiGateway);
		InstanceInfo instanceInfo = application.getInstances().get(0);

		// Have to make use of the API gateway, so using eureka client we get the
		// instance name and port and call the service
		// Also we need to add our Cart Service in API Gateway properties file.
		String productServiceGetOrderURL = ServiceUrlBuilder.constructUrl(nozamaApiGateway, instanceInfo.getPort(),
				"/product/", String.valueOf(productId));

		// http://localhost:8080/product/12
		Product productDetails = restTemplate.getForObject(productServiceGetOrderURL, Product.class);

		LOGGER.info("Product" + productDetails.toString());

		if (productDetails.getProductId().equals(productId)) {

			if (productDetails.getStockQuantity() <= 0) {
				throw new InsufficientStockException("Stock Quantity is empty");
			}
			item.setProduct(productDetails);
		}

		/**
		 * When checking for Quantity and throwing exception, do not catch it. If you
		 * compare this code with previous version, the try catch block was actually
		 * catching the InsufficientStockException when stock quantity exceeds.
		 */
		if (item.getQuantity() <= item.getProduct().getStockQuantity()) {
			try {
				responseItem = addCartService.addCart(item);

				Integer updatedProduct = item.getProduct().getStockQuantity() - item.getQuantity();
				item.getProduct().setStockQuantity(updatedProduct);

				String productServiceUpdateURL = ServiceUrlBuilder.constructUrl(nozamaApiGateway,
						instanceInfo.getPort(), "/product/update", null);
				// rest call for product service update method for updating stock quantity
				restTemplate.put(productServiceUpdateURL, item.getProduct());
				// restTemplate.put("http://localhost:9091/product/update", item.getProduct());
			} catch (Exception e) {
				throw new ItemServiceException("enter less quantity or product does not exist", e);
			}
		} else {
			throw new InsufficientStockException("enter less quantity");
		}

		return ResponseEntity.status(HttpStatus.OK).body(responseItem);
	}

	@ExceptionHandler
	void stockIsLess(InsufficientStockException infe, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), infe.getMessage());
	}

	@ExceptionHandler
	void noProductFoundExceptionHandler(ProductNotFoundException pnfe, HttpServletResponse response)
			throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), pnfe.getMessage());
	}

	@ExceptionHandler
	void stockIsLess(ItemServiceException ise, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ise.getMessage());
	}
}
