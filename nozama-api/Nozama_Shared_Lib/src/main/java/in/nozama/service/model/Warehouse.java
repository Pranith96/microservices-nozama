package in.nozama.service.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "warehouses")
public class Warehouse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "warehouseid")
	Long warehouseid;
	
	@NotNull
	String warehouseCity;
	
	@NotNull
	String warehoustPincode;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userid", nullable = false)
	User thirdPartyUser;

	public Long getWarehouseId() {
		return warehouseid;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseid = warehouseId;
	}

	public String getWarehouseCity() {
		return warehouseCity;
	}

	public void setWarehouseCity(String warehouseCity) {
		this.warehouseCity = warehouseCity;
	}

	public String getWarehoustPincode() {
		return warehoustPincode;
	}

	public void setWarehoustPincode(String warehoustPincode) {
		this.warehoustPincode = warehoustPincode;
	}

	public User getThirdPartyUser() {
		return thirdPartyUser;
	}

	public void setThirdPartyUser(User thirdPartyUser) {
		this.thirdPartyUser = thirdPartyUser;
	}

	@Override
	public String toString() {
		return "Warehouse [warehouseId=" + warehouseid + ", warehouseCity=" + warehouseCity + ", warehoustPincode="
				+ warehoustPincode + ", thirdPartyUser=" + thirdPartyUser + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Warehouse warehouse = (Warehouse) o;
		return warehouseid.equals(warehouse.warehouseid) &&
				Objects.equals(warehouseCity, warehouse.warehouseCity) &&
				Objects.equals(warehoustPincode, warehouse.warehoustPincode) &&
				Objects.equals(thirdPartyUser, warehouse.thirdPartyUser);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), warehouseid, warehouseCity, warehoustPincode, thirdPartyUser);
	}
}
