package in.nozama.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Items service thrown error")
public class ItemServiceException extends Exception {
	
	public ItemServiceException(String message, Exception ex) {
		super(message, ex);
	}

}
