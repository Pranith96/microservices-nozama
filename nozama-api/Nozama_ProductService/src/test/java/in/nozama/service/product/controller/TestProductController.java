package in.nozama.service.product.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.nozama.service.exception.ProductNotFoundException;
import in.nozama.service.model.Category;
import in.nozama.service.model.Product;
import in.nozama.service.product.service.ProductServiceImpl;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestProductController {

	private MockMvc mockMvc;

	@Autowired
	WebApplicationContext context;

	@MockBean
	ProductServiceImpl productServiceImpl;

	ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

	}

	@Test
	public void addProductTest() throws Exception {
		Product mockProduct = new Product();
		mockProduct.setProductId(1L);
		mockProduct.setProductCode("prdct");
		mockProduct.setProductName("mobile");
		mockProduct.setCategory(Category.OTHERS);
		mockProduct.setStockQuantity(20);
		mockProduct.setPricePerItem(20.00);
		mockProduct.setWarehouse(null);
		mockProduct.setProductImg("image");

		String jsonRequest = objectMapper.writeValueAsString(mockProduct);

		Mockito.when(productServiceImpl.addProduct(mockProduct)).thenReturn(mockProduct);

		mockMvc.perform(MockMvcRequestBuilders.post("/product/add").contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest)).andExpect(status().isOk()).andReturn();

		// Mockito.verify(productServiceImpl, Mockito.times(1)).addProduct(mockProduct);

	}

	@Test
	public void getAllProductsTest() throws Exception {

		List<Product> mockproduct = Arrays.asList(
				new Product(1L, "pr1", "mobile", Category.OTHERS, 20, 20.00, null, "image"),
				new Product(2L, "prdct2", "mobile", Category.FOOD, 10, 10.00, null, "image"),
				new Product(3L, "prdct3", "mobile", Category.FURNITURE, 30, 30.00, null, "image"));

		Mockito.when(productServiceImpl.getAllProducts()).thenReturn(mockproduct);

		mockMvc.perform(MockMvcRequestBuilders.get("/product")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(3))).andDo(print());

	}

	@Test
	public void getProductByIdTest() throws Exception {

		Product mockproduct = new Product(1L, "prdct1", "mobile", Category.OTHERS, 20, 20.00, null, "image");

		Mockito.when(productServiceImpl.getProductById(Mockito.anyLong())).thenReturn(mockproduct);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/product/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().toString());

		String expected = "{productId:1,productCode:prdct1,productName:mobile,category:OTHERS,stockQuantity:20,pricePerItem:20.00,warehouse:null,productImg:image}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void updateProductTest() throws Exception {

		Product mockProduct = new Product();
		mockProduct.setProductId(1L);
		mockProduct.setProductCode("prdct");
		mockProduct.setProductName("mobile");
		mockProduct.setCategory(Category.OTHERS);
		mockProduct.setStockQuantity(20);
		mockProduct.setPricePerItem(20.00);
		mockProduct.setWarehouse(null);
		mockProduct.setProductImg("image");

		String jsonRequest = objectMapper.writeValueAsString(mockProduct);

		Mockito.when(productServiceImpl.updateProduct(mockProduct)).thenReturn(mockProduct);

		// we can also do by comparing each values
		/*
		 * mockMvc.perform(MockMvcRequestBuilders.put("/product/update").contentType(
		 * MediaType.APPLICATION_JSON)
		 * .content(jsonRequest).accept(MediaType.APPLICATION_JSON))
		 * .andExpect(jsonPath("$.productId").exists())
		 * .andExpect(jsonPath("$.productCode").exists())
		 * .andExpect(jsonPath("$.productName").exists())
		 * .andExpect(jsonPath("$.category").exists())
		 * .andExpect(jsonPath("$.stockQuantity").exists())
		 * .andExpect(jsonPath("$.pricePerItem").exists())
		 * .andExpect(jsonPath("$.warehouse").exists())
		 * .andExpect(jsonPath("$.productImg").exists())
		 * .andExpect(jsonPath("$.productId").value(1))
		 * .andExpect(jsonPath("$.productCode").value("prdct"))
		 * .andExpect(jsonPath("$.productName").value("mobile"))
		 * .andExpect(jsonPath("$.category").value(Category.OTHERS))
		 * .andExpect(jsonPath("$.stockQuantity").value(20))
		 * .andExpect(jsonPath("$.pricePerItem").value(20.00))
		 * .andExpect(jsonPath("$.warehouse").value(null))
		 * .andExpect(jsonPath("$.productImg").value("image")) .andDo(print());
		 */
		mockMvc.perform(MockMvcRequestBuilders.put("/product/update").contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest)).andExpect(status().isOk()).andReturn();

	}

	@Test
	public void deleteProductByIdTest() throws Exception {
		Product mockProduct = new Product();
		mockProduct.setProductId(1L);
		mockProduct.setProductCode("prdct");
		mockProduct.setProductName("mobile");
		mockProduct.setCategory(Category.OTHERS);
		mockProduct.setStockQuantity(20);
		mockProduct.setPricePerItem(20.00);
		mockProduct.setWarehouse(null);
		mockProduct.setProductImg("image");

		String jsonRequest = objectMapper.writeValueAsString(mockProduct);

		mockMvc.perform(MockMvcRequestBuilders.delete("/product/delete/1").contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest)).andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void testGetProductByIdProductNotFoundException() throws Exception {
		Mockito.when(productServiceImpl.getProductById(Mockito.anyLong())).thenThrow(ProductNotFoundException.class);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/product/3").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		assertEquals(404, result.getResponse().getStatus());
		Mockito.verify(productServiceImpl, Mockito.times(1)).getProductById(3L);

	}

}
