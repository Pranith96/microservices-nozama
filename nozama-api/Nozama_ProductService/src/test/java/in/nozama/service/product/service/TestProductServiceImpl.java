package in.nozama.service.product.service;

import static org.junit.Assert.assertEquals;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import in.nozama.service.exception.ProductNotFoundException;
import in.nozama.service.model.Category;
import in.nozama.service.model.Product;
import in.nozama.service.product.repository.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProductServiceImpl {

	@Autowired
	ProductServiceImpl productServiceImpl;

	@MockBean
	ProductRepository productRepository;

	@Test
	public void addProductTest() {

		Product mockProduct = new Product();
		mockProduct.setProductId(1L);
		mockProduct.setProductCode("prdct");
		mockProduct.setProductName("mobile");
		mockProduct.setCategory(Category.OTHERS);
		mockProduct.setStockQuantity(20);
		mockProduct.setPricePerItem(20.00);
		mockProduct.setWarehouse(null);
		mockProduct.setProductImg("image");

		Mockito.when(productRepository.save(mockProduct)).thenReturn(mockProduct);
		assertEquals(mockProduct, productServiceImpl.addProduct(mockProduct));

	}

	@Test
	public void getAllProductsTest() {

		Mockito.when(productRepository.findAll())
				.thenReturn(Stream
						.of(new Product(1L, "prdct", "mobile", Category.OTHERS, 20, 20.00, null, "image"),
								new Product(2L, "prdct2", "mobile", Category.FOOD, 30, 30.00, null, "image"))
						.collect(Collectors.toList()));

		assertEquals(2, productServiceImpl.getAllProducts().size());
	}

	@Test
	public void getProductByIdTest() throws ProductNotFoundException {
		Long productId = 1L;
		Mockito.when(productRepository.findById(Mockito.anyLong())).thenReturn(
				Optional.ofNullable(new Product(1L, "prdct", "mobile", Category.OTHERS, 20, 20.00, null, "image")));

		Product ProductResult = productServiceImpl.getProductById(productId);
		Mockito.verify(productRepository, Mockito.times(1)).findById(Mockito.anyLong());
		assertEquals(Long.valueOf(productId), ProductResult.getProductId());

	}
	
	// Not yet Completed,code coverage is to be done @Pranith
	@Test(expected = ProductNotFoundException.class)
	public void testGetProductByIdNotFoundException() throws ProductNotFoundException {
		Product product = productServiceImpl.getProductById(3L);
		Mockito.verify(productRepository, Mockito.times(1)).findById(Mockito.anyLong());
	}

	@Test
	public void updateProductTest() {
		Product mockProduct = new Product();
		mockProduct.setProductId(1L);
		mockProduct.setProductCode("prdct");
		mockProduct.setProductName("mobile");
		mockProduct.setCategory(Category.OTHERS);
		mockProduct.setStockQuantity(20);
		mockProduct.setPricePerItem(20.00);
		mockProduct.setWarehouse(null);
		mockProduct.setProductImg("image");

		Mockito.when(productRepository.save(mockProduct)).thenReturn(mockProduct);
		assertEquals(mockProduct, productServiceImpl.updateProduct(mockProduct));
	}

	@Test
	public void deleteProductByIdTest() throws ProductNotFoundException {
		Product mockProduct = new Product();
		mockProduct.setProductId(1L);
		mockProduct.setProductCode("prdct");
		mockProduct.setProductName("mobile");
		mockProduct.setCategory(Category.OTHERS);
		mockProduct.setStockQuantity(20);
		mockProduct.setPricePerItem(20.00);
		mockProduct.setWarehouse(null);
		mockProduct.setProductImg("image");

		productServiceImpl.deleteProductById(mockProduct.getProductId());

		Mockito.verify(productRepository, Mockito.times(1)).deleteById(mockProduct.getProductId());

	}
}
