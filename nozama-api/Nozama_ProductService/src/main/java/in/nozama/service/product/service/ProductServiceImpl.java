package in.nozama.service.product.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nozama.service.exception.ProductNotFoundException;
import in.nozama.service.model.Product;
import in.nozama.service.product.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	ProductRepository productRepository;

	@Override
	public List<Product> getAllProducts() {
		log.info("In Product Service in getAllProducts method");
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(Long productId) throws ProductNotFoundException {
		log.info("In Product Service before getting product details in getProductById method");
		Optional<Product> optionalProduct = productRepository.findById(productId);
		log.info("In Product Service after getting product details in getProductById method");
		if (!optionalProduct.isPresent()) {
			throw new ProductNotFoundException("No Such Product Found");
		}
		return optionalProduct.get();
	}

	@Override
	public Product updateProduct(Product product) {
		log.info("In Product Service in updateProduct method");
		return productRepository.save(product);
	}

	@Override
	public Product addProduct(Product product) {
		log.info("In Product Service in addProduct method");
		return productRepository.save(product);

	}

}
