package in.nozama.service.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.nozama.service.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	Product deleteProductByproductId(Long productId);

}
