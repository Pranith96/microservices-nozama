package in.nozama.service.product.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nozama.service.exception.ProductNotFoundException;
import in.nozama.service.model.Product;
import in.nozama.service.product.service.ProductService;

@RestController
@RequestMapping(value = "/product")
public class ProductController {
	private static final Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	ProductService productService;

	@PostMapping("/add")
	public ResponseEntity<Product> addProduct(@RequestBody Product product) throws ProductNotFoundException {
		log.info("In Product Controller in addProduct method");
		return ResponseEntity.ok(productService.addProduct(product));
	}

	@GetMapping
	public ResponseEntity<List<Product>> getAllProducts() {
		log.info("In Product Controller before getting product details in getAllProducts method");
		List<Product> products = productService.getAllProducts();
		log.info("In Product Controller after getting product details in getAllProducts method");
		return ResponseEntity.ok().body(products);
	}

	@GetMapping("/{productId}")
	public ResponseEntity<Product> getProductById(@PathVariable(name = "productId") Long productId)
			throws ProductNotFoundException {
		log.info("In Product Controller in getProductById method");
		return ResponseEntity.ok(productService.getProductById(productId));
	}

	@PutMapping("/update")
	public ResponseEntity<Product> updateProduct(@RequestBody Product product) throws ProductNotFoundException {
		log.info("In Product Controller in updateProduct method");
		return ResponseEntity.ok(productService.updateProduct(product));
	}

	@ExceptionHandler
	void noProductFoundExceptionHandler(ProductNotFoundException pnfe, HttpServletResponse response)
			throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), pnfe.getMessage());
	}

}
